from django.contrib import admin
from django.urls import path
# 引入app的views
from web.views import home, account

urlpatterns = [
    # =============== 首页的url ===============
    path('', home.index, name="home"),
    # # =============== 登陆相关url ===================
    path('login/', account.user_login, name="login"),  # 用户登录
    path('smslogin/', account.user_sms_login, name="sms_login"),  # 用户手机验证码登录
    path('register/', account.user_register, name="register"),  # 用户注册
    path('resetpassword/', account.user_reset_password, name="resetpassword"),  # 用户重置密码
    path('image/code/', account.image_code, name="image_code"),  # 图片验证码
    path('sms/send/', account.sms_send, name="sms_send"),  # 发送短信的接口
    path('logout/', account.user_logout, name="logout"),  # 用户注销

    path('demo/', account.user_demo, name='demo'),

]
